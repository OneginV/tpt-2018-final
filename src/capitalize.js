module.exports = function capitalize(input) {
    if(typeof input !== 'string'){
        throw new Error('Pls meeeeen, string only, XD')
    }
    return input.charAt(0).toUpperCase() + input.slice(1);
}