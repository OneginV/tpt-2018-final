var config = require('../../nightwatch.conf.js');

module.exports = {
    'google': function(browser) {
        browser
            .url('https://www.google.ee/')
            .waitForElementVisible('body')
            .setValue('input[class="gLFyf gsfi"]', ['Tallinn', browser.Keys.ENTER]
            .assert.conteinsText('Tallinn')
            .pause(2000)
            .saveScreenshot(`${config.imgpath(browser)}google1.png`)
            .pause(2000)
            .click('a[href="https://www.tallinn.ee/"]')
            .saveScreenshot(`${config.imgpath(browser)}google2.png`)
            .pause(2000)
            .end();
        
    }
};