var config = require('../../nightwatch.conf.js');

module.exports = {
    'tpt': function(browser) {
        browser
            .resizeWindow(1920, 1080)
            .url('https://www.tptlive.ee/')
            .saveScreenshot(`${config.imgpath(browser)}tpt1.png`)
            .waitForElementVisible('body')
            .pause(2000)
            .click('li[id="menu-item-1313"]')
            .saveScreenshot(`${config.imgpath(browser)}tpt2.png`)
            .waitForElementVisible('body')
            .click('a[href="https://tpt.siseveeb.ee/veebivormid/tunniplaan/tunniplaan?oppegrupp=226&nadal=03.12.2018"]')
            .saveScreenshot(`${config.imgpath(browser)}tpt3.png`)
            .pause(2000)
            .end();
        
    }
};