const capitalize = require('../../src/capitalize');

describe('sum', () => {
  it('triin => Triin', () => {
    expect(capitalize('triin')).toBe("Triin");
  });
  it('error - Pls meeeeen, string only, XD', () => {
    expect(() => {
      capitalize({ "word": 'cat' });
    }).toThrow(/Pls meeeeen, string only, XD/);
  });
});
